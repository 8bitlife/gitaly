package git2go

// Error strings present in the legacy Ruby implementation
const (
	LegacyErrPrefixInvalidBranch        = "Invalid branch"
	LegacyErrPrefixInvalidSubmodulePath = "Invalid submodule path"
	LegacyErrPrefixFailedCommit         = "Failed to create commit"
)
